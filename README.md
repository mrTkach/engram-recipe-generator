Hello Tech Team of Engram!

To perform current task I was using React without any supported library. <br />
Current application is using simple API GET request to "https://developer.edamam.com" to fetch recipes by search word.<br />
All incoming data is stored locally in useState() hook. I would implement Redux/RTK to store all data in case I have more time.<br />
But we can discuss that option and others during our conversation.<br />
To have all storing logic in one place - I've placed them into useFetchData() custom hook, to have <App /> more clean and readable.<br />
For the styling I've used regular scss. As an idea for implementation - to use MaterialUI / BootstrapUI / ChakraUI or similar libraries.<br />
Current code can be refactored in a better way, but idea is to show the way I think.<br />

For my own use and just for sharing I added screenshot of responded Promise object.<br />

Curent project has been deployed to gitlab repository on the following link:  https://gitlab.com/mrTkach/engram-recipe-generator <br />

I'm completely ready and open for the critic and feedback, regarding tech task.

<hr />

To have project running locally – just install all dependencies by typing in terminal "npm install" and run by typing "npm run dev". <br/>
There is no need to install project locally as it has been deployed to Vercel and is available by link https://engram-recipe-generator.vercel.app/
<hr />



Aufgabe: Erstelle eine interaktive "Random Recipe Generator"-Anwendung.

Anforderungen:
1. Die Anwendung soll eine Benutzeroberfläche haben, auf der Benutzer auf eine Schaltfläche klicken können, um zufällige Rezepte anzuzeigen.
2. Die Rezepte sollen aus einer vordefinierten Liste von Gerichten oder Lebensmitteln stammen. Du kannst eine API verwenden, um die Rezepte abzurufen oder eine statische Liste von Rezepten in der Anwendung bereitstellen.
3. Jedes Rezept soll eine Bildvorschau, den Namen des Gerichts, die Zutatenliste und eine Schritt-für-Schritt-Anleitung zur Zubereitung enthalten.
4. Implementiere eine Filterfunktion, mit der Benutzer nach bestimmten Kriterien suchen können, z. B. nach Küchentyp (z. B. italienisch, chinesisch, mexikanisch), Schwierigkeitsgrad oder Hauptzutat.
5. Die Anwendung soll responsive sein und auf verschiedenen Bildschirmgrößen gut funktionieren.

Technologien:

- Verwende HTML, CSS und JavaScript für die Frontend-Entwicklung. Du kannst eine JavaScript-Bibliothek oder ein Framework wie React, Vue.js oder Angular verwenden.
- Wenn du eine API verwendest um Rezepte abzurufen, benötigst du möglicherweise AJAX oder Fetch-API-Anfragen.

Bitte investiere nicht mehr als 2 Stunden in die Bearbeitung der Aufgabe. Wir interessieren uns auch für Teilergebnisse!


![img.png](img.png)