import styles from "./SingleCard.module.scss";
import noPreview from "../../assets/imgBlocker.jpg";

const SingleCard = ({ item: { recipe } }) => {
  const { image, label, ingredientLines, cuisineType } = recipe;
  const style = {
    backgroundImage: `url(${image || noPreview})`,
  };

  return (
    <div className={styles.card}>
      <div className={styles.image} style={style} />
      <div className={styles.description}>
        <h2>{label}</h2>
        <ul className={styles.cuisine}>
          {cuisineType?.map((el, index) => (
            <li key={index}>{el} cuisine</li>
          ))}
        </ul>
        <ol className={styles.list}>
          {ingredientLines?.map((item, index) => (
            <li key={index} className={styles.listItem}>
              {item}
            </li>
          ))}
        </ol>
      </div>
    </div>
  );
};

export default SingleCard;
