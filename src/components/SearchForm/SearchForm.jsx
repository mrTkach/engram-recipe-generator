import styles from "./SearchForm.module.scss";

const SearchForm = ({ getSearch, updateSearch, search }) => (
  <form className={styles.searchForm} onSubmit={getSearch}>
    <input
      type="text"
      className={styles.searchBar}
      onChange={updateSearch}
      value={search}
    />
    <button type="submit" className={styles.button}>
      Search
    </button>
  </form>
);

export default SearchForm;
