import { useEffect, useState } from "react";
import SingleCard from "../SingleCard/SingleCard.jsx";
import FilterButtons from "../FilterButtons/FilterButtons.jsx";
import styles from "./RecipesSection.module.scss";

const RecipesSection = ({ recipes }) => {
  const [cuisineTypes, setCuisineTypes] = useState([]);
  const [filteredCards, setFilteredCards] = useState(recipes);
  const [isDefaultSelected, setDefaultSelect] = useState(true);

  const cuisineUniqueTypesExtractor = () => {
    const selectedTypes = recipes?.map((el) => el.recipe.cuisineType);
    const extractedElements = selectedTypes.flatMap((item) =>
      Array.isArray(item) ? item : []
    );
    const uniqArray = Array.from(new Set(extractedElements));
    setCuisineTypes(uniqArray);
    setFilteredCards(recipes);
  };

  useEffect(() => {
    cuisineUniqueTypesExtractor();
    setDefaultSelect(true);
  }, [recipes]);

  return (
    <>
      <FilterButtons
        cuisineTypes={cuisineTypes}
        recipes={recipes}
        isDefaultSelected={isDefaultSelected}
        setFilteredCards={setFilteredCards}
        setDefaultSelect={setDefaultSelect}
      />
      <section className={styles.recipesSection}>
        {filteredCards.map((item, index) => (
          <SingleCard item={item} key={index} />
        ))}
      </section>
    </>
  );
};

export default RecipesSection;
