import styles from "./FilterButtons.module.scss";

const FilterButtons = ({
  cuisineTypes,
  setFilteredCards,
  recipes,
  isDefaultSelected,
  setDefaultSelect,
}) => {
  const onAllClickHandler = () => {
    setFilteredCards(recipes);
    setDefaultSelect(true);
  };

  const onItemClickHandler = (el) => {
    setDefaultSelect(false);
    setFilteredCards(
      recipes.filter((item) => item.recipe.cuisineType.includes(el))
    );
  };

  return (
    <div className={styles.filterButtons}>
      <p>
        <input
          type="radio"
          id="all"
          name="radio-group"
          checked={isDefaultSelected}
          onChange={onAllClickHandler}
        />
        <label htmlFor="all">ALL</label>
      </p>
      {cuisineTypes.map((el, index) => (
        <p key={index}>
          <input
            type="radio"
            onChange={() => onItemClickHandler(el)}
            name="radio-group"
            id={el}
          />
          <label htmlFor={el}>{el.toUpperCase()}</label>
        </p>
      ))}
    </div>
  );
};

export default FilterButtons;
