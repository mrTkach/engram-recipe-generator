import React, { useEffect } from "react";
import { useFetchData } from "./hooks/useFetchData.js";
import SearchForm from "./components/SearchForm/SearchForm.jsx";
import RecipesSection from "./components/RecipesSection/RecipesSection.jsx";
import EngramLogo from "./assets/logo-engram.svg";
import styles from "./App.module.scss";

const App = () => {
  const { recipes, search, query, getRecipes, getSearch, updateSearch } =
    useFetchData();

  useEffect(() => {
    getRecipes();
  }, [query]);

  return (
    <div className={styles.App}>
      <div className={styles.container}>
        <a href="/" className={styles.logo}>
          <img src={EngramLogo} alt="EngramLogo" />
        </a>
        <h1>Random Recipe Generator</h1>
        <SearchForm
          getSearch={getSearch}
          updateSearch={updateSearch}
          search={search}
        />
        {!recipes.length ? (
          <p>We found nothing. Try again please</p>
        ) : (
          <RecipesSection recipes={recipes} />
        )}
      </div>
    </div>
  );
};

export default App;
