import { useState } from "react";
import { APP_ID, APP_KEYS, BASE_URL } from "../constats.js";

export const useFetchData = () => {
  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState("");
  const [query, setQuery] = useState("Pinkel");

  const getRecipes = async () => {
    const response = await fetch(
      `${BASE_URL}?q=${query}&app_id=${APP_ID}&app_key=${APP_KEYS}`
    );
    const data = await response.json();
    setRecipes(data?.hits);
  };

  const updateSearch = (e) => {
    setSearch(e.target.value);
  };

  const getSearch = (e) => {
    e.preventDefault();

    setQuery(search);
    setSearch("");
  };

  return {
    recipes,
    search,
    query,
    getRecipes,
    getSearch,
    updateSearch,
  };
};
